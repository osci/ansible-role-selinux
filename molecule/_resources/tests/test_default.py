import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("ns_servers")


# this is enforced on the host, so we cannot test the status after reboot on a container
def test_selinux(host):
    assert host.file("/etc/selinux/config").contains("enforcing")


# it was possible to run auditd up to some update of CS9
# by setting local_events=no and priority_boost=0 but that is not longer the case
#
# def test_service(host):
#     srv = host.service("auditd")
#     assert srv.is_running
#     assert srv.is_enabled
